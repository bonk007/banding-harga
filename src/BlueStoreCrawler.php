<?php

namespace BandingHarga;

/**
 * Indomaret
 */

 class BlueStoreCrawler implements CrawlerInterface
 {
     /**
      * Base URI
      * @var string;
      */
     protected $URI = "http://www.klikindomaret.com";

     /**
      * Full URL
      * @var string;
      */
     protected $URL;

     /**
      * DOM Results
      * @var \GuzzleHttp\Psr7\Stream;
      */
     protected $DomResult;

     /**
      * Http Client
      * @var \GuzzleHttp\Client;
      */
     protected $HttpClient;

     /**
      * Dom object
      * @var \DOMDocument
      */
     protected $DomObject;

     /**
      * Init class
      * @return void
      */
     public function __construct()
     {
         $this->SetHttpClient();
         $this->SetDomObject();
     }

     /**
      * Init Http Client
      * @return void
      */
     protected function SetHttpClient()
     {
         $this->HttpClient = new \GuzzleHttp\Client;
     }

     /**
      * Init DOM Object
      * @return void
      */
     protected function SetDomObject()
     {
         $this->DomObject = new \simple_html_dom();
     }

     /**
      * Get crawling items result
      * @return array
      */
     public function GetItems()
     {
         $DOM = $this->GetDomResult();
         $this->DomObject->load($DOM);
         $Items = $this->DomObject->find("a.comparable");

         $Collections = array();

         if(count($Items) < 1)
            return $Collections;

        foreach($Items as $Element)
        {

            $Detail = array(
                "id" => $Element->find("#itemId", 0)->getAttribute("value"),
                "url" => $this->URI . $Element->href,
                "image_url" => $Element->find("img.lazy", 0)->getAttribute("data-original"),
                "name" => $Element->find("h5.producttitle", 0)->innertext(),
                "original_price" => $this->ClearNumberFormatter($Element->find("span.price b", 0)->innertext()),
                "discount" => 0,
                "discount_price" => 0
            );

            $Discount = $Element->find("b.discount", 0);
            $IsDiscount = !empty($Discount);
            if($IsDiscount) {
                $Detail["discount_price"] = $Detail["original_price"];
                $Detail["original_price"] = $this->ClearNumberFormatter($Element->find("s b", 0)->innertext());
                $Detail["discount"] = $this->ClearNumberFormatter($Discount->innertext());
            }

            array_push($Collections, $Detail);
        }

        return $Collections;

     }

     /**
      * Clearing not numbering value
      * @param string $value
      * @return string
      */
     protected function ClearNumberFormatter($Value)
     {
         return preg_replace_callback('/[\D]+/', function($str)
         {
             return null;
         }, $Value);
     }

     /**
      * Get initialized Http Client
      * @return \GuzzleHttp\Client
      */
     public function GetHttpClient()
     {
         return $this->HttpClient;
     }

     /**
      * Init URL
      * @param array $Query
      * @return \Bonk007\BandingHarga\CrawlerInterface
      */
     protected function SetUrl(array $Query)
     {
         $this->URL = $this->URI .  "/search/" . $this->GetQueryString($Query);

         return $this;
     }

     /**
      * Make request to crawling URL target
      * @return \GuzzleHttp\Psr7\Response
      */
     protected function MakeRequest()
     {
         $Url = $this->URL;
         return $this->GetHttpClient()->request("GET", $Url);
     }

     /**
      * Get DOM stream
      * @return \GuzzleHttp\Psr7\Stream
      */
     public function GetDomResult()
     {
         return $this->DomResult;
     }

     /**
      * Initialize crawling stream result
      * @return \Bonk007\BandingHarga\CrawlerInterface
      */
     public function Search(array $Query)
     {
          $this->DomResult = $this->SetUrl($Query)->MakeRequest()->getBody();
          return $this;
     }

     /**
      * Get query string
      * @param array $Query
      * @return string
      */
     protected function GetQueryString(array $Query)
     {
         $QueryString = "?";

         foreach($Query as $Key => $Value)
            $QueryString .= $Key . "=" . $Value . "&";

        return rtrim($QueryString, "&");
     }
 }

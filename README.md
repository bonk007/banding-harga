Banding Harga
============
Perkenalan
----------------
**Banding Harga** adalah library untuk mendapatkan informasi produk sesuai dengan pencarian user. Informasi yang diterima didapat dari konten layanan toko *online* dari *franchise retail* terbesar di Indonesia (Si Biru dan Si Merah - *You know who they are*).
Instalasi
-------------------------
Tambahkan baris-baris ini ke dalam file composer.json project

    "repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:bonk007/banding-harga.git"
        }
    ]

Setelah itu jalan kan perintah `composer update`.

Cara Penggunaan
-------------------------

    use BandingHarga\BlueStoreCrawler;

    $SiBiru = new BlueStoreCrawler();

    $SiBiru->Search(array(
	    "key" => "Oreo",
	    "pagesize" => 96,
	    "sortcol" => "promo"
	));
	echo json_encode($SiBiru->GetItems());
